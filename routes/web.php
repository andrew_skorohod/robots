<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use Illuminate\Http\Request;
use GuzzleHttp\Client;

Route::get('/', function () {//startpage
    return view('main');
});

Route::get('/ajax/geturl',['middleware' => 'cors', function (Request $request) {
    $link = $request->link;//get validated link input
    $client = new Client(['http_errors' => false]);//don't broke if not 200
    $res = $client->get($link."robots.txt");//making request
    if ($res->getStatusCode() == 200){//check response
        $content_length = $res->getHeader('Content-Length');
        $body = $res->getBody()->getContents();//convert to string
        $host = check_host($body);// validation functions
        $sitemap = check_sitemap($body);
        $weight = check_length($content_length[0]);
        return response()->json(['check1' => 'ok',//return each case in json
            'check6' => $host[0],
            'check8' => $host[1],
            'check10' => $weight,
            'check11' => $sitemap,
            'check12' => 'ok',
            'status'=>200,
            ]);
    }
    else {//no document or wrong link
        return response()->json(['check1' => 'error',
            'status'=>$res->getStatusCode()]);//404 or whatever
    }
}]);

function check_host($a) {
    $count = substr_count($a, 'Host'); 
    if ($count == 0) {
        return ['error','error'];
    }
    elseif ($count == 1)  {
        return ['ok','ok'];
    } 
    else {
        return ['ok','error'];
    }
}

function check_sitemap($a){
    if (substr_count($a, 'Sitemap') > 0) {
        return 'ok';
    }
    else {
        return 'error';
    }
}

function check_length($a) {
    if ($a>32768) {//32 kb
        return ['error',$a];
    }
    else {
        return ['ok',$a];
    }
}
