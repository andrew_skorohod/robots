angular.module('MyApp', [])
  .controller('MyCtrl', ['$scope', '$http', function ($scope, $http) {
      $scope.website = '';
      $scope.checks = [//skeleton of responses
        {'name':'Проверка наличия файла robots.txt',
          'status1':'Ок','status2':'Ошибка',
          'state1':'Файл robots.txt присутствует',
          'state2': 'Файл robots.txt отсутствует',
          'rec1': 'Доработки не требуются',
          'rec2': 'Программист: Создать файл robots.txt и разместить его на сайте.'
        },
        {'name':'Проверка указания директивы Host',
          'status1':'Ок','status2':'Ошибка',
          'state1':'Директива Host указана',
          'state2': 'В файле robots.txt не указана директива Host',
          'rec1': 'Доработки не требуются',
          'rec2': 'Программист: Для того, чтобы поисковые системы знали, \n\
какая версия сайта является основных зеркалом, необходимо \n\
прописать адрес основного зеркала в директиве Host.\n\
 В данный момент это не прописано. Необходимо добавить в файл robots.txt директиву Host.\n\
 Директива Host задётся в файле 1 раз, после всех правил.'
        },
        {'name':'Проверка количества директив Host, прописанных в файле',
          'status1':'Ок','status2':'Ошибка',
          'state1':'В файле прописана 1 директива Host',
          'state2': 'В файле прописано несколько директив Host',
          'rec1': 'Доработки не требуются',
          'rec2': 'Программист: Директива Host должна быть указана в файле толоко 1 раз. \n\
Необходимо удалить все дополнительные директивы Host и оставить только 1, \n\
корректную и соответствующую основному зеркалу сайта'
        },
        {'name':'Проверка размера файла robots.txt',
          'status1':'Ок','status2':'Ошибка',
          'state1':'Размер файла robots.txt составляет ',
          'state10':', что находится в пределах допустимой нормы',
          'state2': 'Размера файла robots.txt составляет ',
          'state20': ', что превышает допустимую норму',
          'rec1': 'Доработки не требуются',
          'rec2': 'Программист: Максимально допустимый размер файла robots.txt составляем 32 кб. \n\
Необходимо отредактировть файл robots.txt таким образом, чтобы его размер не превышал 32 Кб'
        },
        {'name':'Проверка указания директивы Sitemap',
          'status1':'Ок','status2':'Ошибка',
          'state1':'Директива Sitemap указана',
          'state2': 'В файле robots.txt не указана директива Sitemap',
          'rec1': 'Доработки не требуются',
          'rec2': 'Программист: Добавить в файл robots.txt директиву Sitemap'
        },
          {'name':'Проверка кода ответа сервера для файла robots.txt',
          'status1':'Ок','status2':'Ошибка',
          'state1':'Файл robots.txt отдаёт код ответа сервера 200',
          'state2': 'При обращении к файлу robots.txt сервер возвращает код ответа ',
          'rec1': 'Доработки не требуются',
          'rec2': 'Программист: Файл robots.txt должны отдавать код ответа 200,\n\
 иначе файл не будет обрабатываться. Необходимо настроить сайт таким образом,\n\
 чтобы при обращении к файлу robots.txt сервер возвращает код ответа 200'
        }
      ];
      var url = $('.navbar-brand').attr('href')+'/ajax/geturl';//get base url
      
      $scope.search = function () {
          
        function getDomain(url){//url vilidator and purifier
            domain=url.split("//")[1];
            return url.split("//")[0]+'//'+domain.split("/")[0]+'/';
          }  
          
        function handler(data){//change angularjs view depending on response
            if (data.status != 200){
                console.log(typeof data.status);
                $scope.checks[0].status=$scope.checks[0].status2;
                $scope.checks[0].state=$scope.checks[0].state2;
                $scope.checks[0].rec=$scope.checks[0].rec2;
                $scope.checks[5].status=$scope.checks[5].status2;
                $scope.checks[5].state=$scope.checks[5].state2+data.status;
                $scope.checks[5].rec=$scope.checks[5].rec2;
            }
            else {
                $scope.checks[0].status=$scope.checks[0].status1;
                $scope.checks[0].state=$scope.checks[0].state1;
                $scope.checks[0].rec=$scope.checks[0].rec1;
                
                if (data.check6==='ok'){
                $scope.checks[1].status=$scope.checks[1].status1;
                $scope.checks[1].state=$scope.checks[1].state1;
                $scope.checks[1].rec=$scope.checks[1].rec1;
                }
                else {
                $scope.checks[1].status=$scope.checks[1].status2;
                $scope.checks[1].state=$scope.checks[1].state2;
                $scope.checks[1].rec=$scope.checks[1].rec2;
                }
                if (data.check8==='ok'){
                $scope.checks[2].status=$scope.checks[2].status1;
                $scope.checks[2].state=$scope.checks[2].state1;
                $scope.checks[2].rec=$scope.checks[2].rec1;
                }
                else{
                $scope.checks[2].status=$scope.checks[2].status2;
                $scope.checks[2].state=$scope.checks[2].state2;
                $scope.checks[2].rec=$scope.checks[2].rec2;
                }
                if(data.check10[0]==='ok'){
                $scope.checks[3].status=$scope.checks[3].status1;
                $scope.checks[3].state=$scope.checks[3].state1+data.check10[1]+$scope.checks[3].state10;
                $scope.checks[3].rec=$scope.checks[3].rec1;
                }
                else {
                $scope.checks[3].status=$scope.checks[3].status2;
                $scope.checks[3].state=$scope.checks[3].state2+data.check10[1]+$scope.checks[3].state20;
                $scope.checks[3].rec=$scope.checks[3].rec2;
                }
                if (data.check11==='ok'){
                $scope.checks[4].status=$scope.checks[4].status1;
                $scope.checks[4].state=$scope.checks[4].state1;
                $scope.checks[4].rec=$scope.checks[4].rec1;
                }
                else{
                $scope.checks[4].status=$scope.checks[4].status2;
                $scope.checks[4].state=$scope.checks[4].state2;
                $scope.checks[4].rec=$scope.checks[4].rec2;
                }
                $scope.checks[5].status=$scope.checks[5].status1;
                $scope.checks[5].state=$scope.checks[5].state1;
                $scope.checks[5].rec=$scope.checks[5].rec1;
            }
        }
        $http({//making ajax request
            url: url, 
            method: "GET",
            params: {link: getDomain($scope.website)}//sending clear url
            }).then(function mySucces(response) {
                console.log(response);//just for debugging
                handler(response.data);
            }, function myError(response) {
                console.log(response.statusText);
            });
      }
  }]);