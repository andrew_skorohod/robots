@extends('layouts.app')

@section('content')
<div ng-app="MyApp" ng-controller="MyCtrl">
  <!-- call $scope.search() when submit is clicked. -->
  <form ng-submit="search()" class="form-group">
    <!-- will automatically update $scope.user.first_name and .last_name -->
    <label>Enter here valid url</label>
    <input type="url" ng-model="website" class='form-control'> 
    <input type="submit" value="Search" class="btn btn-default">
  </form>

  <div>
    <table class="table table-bordered">
        <thead> 
            <tr>
                <th>Название проверки</th>
                <th>Статус</th>
                <th>Текущее состояние</th>
                <th>Рекомендации</th>
            </tr>    
        </thead>
        
        <tbody>
            <tr ng-repeat="check in checks">
                <td class="text-center">@{{check.name }}</td>
                <td class="text-center">@{{check.status }}</td>
                <td class="text-center">@{{check.state }}</td>
                <td class="text-center">@{{check.rec }}</td>
            </tr>
        </tbody>
    </table>
  </div>

</div>




@endsection